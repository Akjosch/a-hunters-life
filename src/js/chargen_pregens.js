setup.pregens = {};
State.variables.gens = {};

setup.pregenFunction = function(name, filter) {
	var promise = undefined;

	return function(clear) {
		if(clear) {
			promise = undefined;
		} else if(!promise) {
			if(State.variables.gens[name] && State.variables.gens[name].id) {
				promise = new setup.InspectablePromise(function(resolve) { resolve(new setup.Character(State.variables.gens[name].id)); });
			} else {
				promise = setup.promiseFiltered(setup.Character.newSequentialGen(), filter);
			}
		}
		return promise;
	};
};

setup.pregensClear = function() {
	Object.values(setup.pregens).forEach(function(f) { f(true); });
};

setup.pregens.visitor = setup.pregenFunction("visitor", (c) =>
	c.sex === setup.Sex.FEMALE &&
	c.stats._average > 0 &&
	c.stats.toughness > 15 &&
	c.stats.willpower < -50 &&
	c.stats.appearance > 0 &&
	c.race === setup.Race.HESCIMAN &&
	c.bmi < 20
);

jQuery(document).on(':passageinit', function () {
	var gens = State.variables.gens = State.variables.gens || {};
	Object.keys(setup.pregens).forEach(function(key) {
		if(!gens[key]) {
			var promise = setup.pregens[key]();
			if(promise.resolved) {
				gens[key] = promise.value;
			}
		}
	});
});
