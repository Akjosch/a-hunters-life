/**
 * @param {any} val
 * @returns {boolean}
 */
function isObject(val) {
	var vtype = typeof val;
	return null != val && ("object" === vtype || "function" === vtype);
}

class Skill {	
	/**
	 * @param {string} skill
	 * @param {string=} attribute
	 * @param {{ tags?: string[]; }=} options
	 */
	constructor(skill, attribute, options) {
		options = options || {};
		this.id = String(skill);
		this.attribute = attribute ? String(attribute) : undefined;
		this.options = Object.assign({}, options);
		this.tags = options.tags || [];
		delete this.options.tags;
	}

	/**
	 * @param {string} t
	 */
	hasTag(t) {
		return this.tags.includes(t);
	}

	/**
	 * @param {string[]} ts
	 */
	hasAnyTag(ts) {
		return this.tags.includesAny(ts);
	}

	_init(character) {
		if(!isObject(character)) {
			return false;
		}
		if(character.skills && !isObject(character.skills)) {
			return false;
		}
		character.skills = character.skills || {};
		character.skills[this.id] = character.skills[this.id] || 0;
		character.skills[this.id + "Mods"] = character.skills[this.id + "Mods"] || {};
		return true;
	}

	add(character) {
		this._init(character);
	}

	clearCache(character) {
		if(this.has(character)) {
			delete character.skills[this.id + "CachedValue"];
			delete character.skills[this.id + "CachedMods"];
		}
	}

	/**
	 * @param {string} id
	 * @param {number|object} mod
	 */
	addMod(character, id, mod) {
		if(this._init(character)) {
			id = String(id);
			if(Number.isFinite(mod)) {
				mod = { add: mod };
			}
			if(typeof mod === "object") {
				character.skills[this.id + "Mods"][id] = mod;
				this.clearCache(character);
			}
		}
	}

	/**
	 * @param {String} id
	 */
	removeMod(character, id) {
		if(this._init(character)) {
			character.skills[this.id + "Mods"][String(id)];
			this.clearCache(character);
		}
	}

	clearMods(character) {
		if(this.has(character)) {
			character.skills[this.id + "Mods"] = {};
			this.clearCache(character);
		}
	}

	_calcTotalMods(character) {
		if(this.has(character)) {
			character.skills[this.id + "CachedMods"] = Object.values(character.skills[this.id + "Mods"]).reduce(function(result, m) {
				if(Number.isFinite(m)) {
					result.add += m;
				} else {
					result.add += m.add || 0;
					result.mult += m.mult || 0;
					result.baseAdd += m.baseAdd || 0;
					result.baseMult += m.baseMult || 0;
				}
				return result;
			}, {add: 0, mult: 0, baseAdd: 0, baseMult: 0})
		}
	}

	raw(character) {
		if(!this.has(character)) {
			return undefined;
		}
		var base = character.skills[this.id];
		if(character.skills[this.id + "CachedValue"] === undefined) {
			this._calcTotalMods(character);
			var unifiedMod = character.skills[this.id + "CachedMods"];
			character.skills[this.id + "CachedValue"] = (base * (1 + unifiedMod.baseMult) + unifiedMod.baseAdd) * (1 + unifiedMod.mult) + unifiedMod.add
		}
		return {
			base: base,
			value: character.skills[this.id + "CachedValue"],
			mod: unifiedMod
		};
	}

	get(character) {
		var raw = this.raw(character);
		return raw ? raw.value : undefined;
	}

	has(character) {
		return isObject(character) && isObject(character.skills) && Number.isFinite(character.skills[this.id]);
	}

	check(character, rnd, mod) {
		rnd = rnd || Math.random;
		if(!this.has(character)) {
			return clone(Skill._autoFail);
		}
		var skillLevel = this.get(character) + (Number.isFinite(mod) ? mod : 0);
		/* Generating a Gaussian random variable using Box-Muller transform */
		var u = 1 - rnd();
		var v = 1 - rnd();
		var roll = 50 + Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v) * 25;
		var rawCritLimit = Math.min(0, skillLevel);
		var rawFumbleLimit = Math.max(100, skillLevel);
		var rawResult = skillLevel - roll;
		return {
			roll: roll,
			result: rawResult,
			critical: (roll <= rawCritLimit || rawResult >= 80),
			fumble: (roll >= rawFumbleLimit || rawResult <= -80),
			success: (roll <= rawCritLimit || rawResult >= 0),
			failure: (roll >= rawFumbleLimit || rawResult < 0)
		};
	}
}
Skill._autoFail = Object.freeze({ roll: -50, result: -50, critical: false, fumble: false, success: false, failure: true });
setup.Skill = Skill;