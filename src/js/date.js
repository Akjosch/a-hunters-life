setup.date = (function() {
	const YEAR_LENGTH = 357;
	/* month and their start days */
	const MONTHS = ["Perim", "Sectar", "Triael", "Quantas", "Pentim", "Hekos", "Sepeter", "Oktaer", "Niunim"];
	const START_DAYS = [0, 35, 77, 119, 154, 196, 238, 273, 315];
	/* moon parameters */
	const MOONS = ["Eweekia", "Medainos"];
	const MOON_PERIODS = [7.00000, 46.79034];
	const MOON_OFFSETS = [0.00000, 12.43972];

	function print(day) {
		day = Math.floor(Number(day) % YEAR_LENGTH);
		if(day < 0) { day += YEAR_LENGTH; }
		var idx = 0;
		while(idx < START_DAYS.length && START_DAYS[idx + 1] <= day) {
				++ idx;
		}
		return (day - START_DAYS[idx] + 1) + ". " + MONTHS[idx];
	}

	function yearPrint(day) {
		return Math.floor(day / YEAR_LENGTH);
	}

	function fullPrint(day) {
		return print(day) + " " + yearPrint(day);
	}

	return {
		print: print,
		yearPrint: yearPrint,
		fullPrint: fullPrint,
		moondata: function(day) {
			return [0, 1].map((midx) => {
				const dayOfMoon = (day - MOON_OFFSETS[midx]) % MOON_PERIODS[midx];
				return {
					name: MOONS[midx],
					phase: Math.floor(dayOfMoon / MOON_PERIODS[midx] * 4),
					day: Math.round(dayOfMoon)
				};
			});
		}
	};
})();