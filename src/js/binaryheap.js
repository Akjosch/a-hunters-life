setup.BinaryHeap = (function() {
	const _content = Symbol("content");
	const _scoring = Symbol("scoring function");

	/**
	 * @param {T[]} content 
	 * @param {(value:T) => number} scoring}
	 * @param {number} pos
	 * @template T
	 */
	function bubbleUp(content, scoring, pos) {
		var element = content[pos];
		var score = scoring(element);
		while(pos > 0) {
			var parentIdx = Math.floor((pos + 1) / 2) - 1;
			var parent = content[parentIdx];
			if(score >= scoring(parent)) {
				break;
			}
			content[parentIdx] = element;
			content[pos] = parent;
			pos = parentIdx;
		}
	}

	/**
	 * @param {T[]} content 
	 * @param {(value:T) => number} scoring}
	 * @param {number} pos
	 * @template T
	 */
	function sinkDown(content, scoring, pos) {
		var length = content.length;
		var element = content[pos];
		var elementScore = scoring(element);

		// eslint-disable-next-line no-constant-condition
		while(true) {
			var child2Idx = (pos + 1) * 2;
			var child1Idx = child2Idx - 1;
			/** @type {number} */
			var swap = NaN;
			if(child1Idx < length) {
				var child1 = content[child1Idx];
				var child1Score = scoring(child1);
				if(child1Score < elementScore) {
					swap = child1Idx;
				}
			}
			if(child2Idx < length) {
				var child2 = content[child2Idx];
				var child2Score = scoring(child2);
				if(child2Score < (Number.isNaN(swap) ? elementScore : child1Score)) {
					swap = child2Idx;
				}
			}
			if(Number.isNaN(swap)) {
				break;
			}
			content[pos] = content[swap];
			content[swap] = element;
			pos = swap;
		}
	}

	/**
	 * @template T
	 */
	class BinaryHeap {
		/**
		 * @param {(value:T) => number} scoring 
		 */
		constructor(scoring) {
			this[_content] = [];
			if(typeof scoring === "function") {
				this[_scoring] = scoring;
			}
		}

		/**
		 * @param {T} element 
		 */
		push(element) {
			this[_content].push(element);
			bubbleUp(this[_content], this[_scoring], this[_content].length - 1);
		}

		/**
		 * @param {T} element 
		 */
		rescore(element) {
			var elementIdx = this[_content].indexOf(element);
			if(elementIdx >= 0) {
				sinkDown(this[_content], this[_scoring], elementIdx);
			}
		}
		
		/**
		 * @returns {T}
		 */
		pop() {
			/** @type {T} */
			var result = this[_content][0];
			/** @type {T} */
			var end = this[_content].pop();
			if(this[_content].length > 0) {
				this[_content][0] = end;
				sinkDown(this[_content], this[_scoring], 0);
			}
			return result;
		}

		/**
		 * @returns {T}
		 */
		peek() {
			return this[_content][0];
		}

		get length() {
			return this[_content].length;
		}
	}

	return BinaryHeap;
})();