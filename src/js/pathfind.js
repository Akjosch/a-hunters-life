/**
 * Use Dijkstra's algorithm to find the lowest-cost route between start and target nodes
 * given a graph implementation which provides us with a suitable way to query
 * the neighbors of a node as well as the cost of traversing from one (source) to
 * a connected next (target) node.
 * 
 * Note that this implementation relies on the individual nodes being equal under
 * strict equality (===). Using primitive types like strings and numbers for node
 * identification is safe, everything else might not be.
 * 
 * The code relies on ES5 and ES6 features like the Set and Map datatypes to exist,
 * as well as the "for ... of" iteration working on Maps. Use Babel and shims of your
 * choice to make it work on IE11 or older. SugarCube provides all the shims you
 * need, in particular.
 * 
 * @param {{ neighbors: (note: T) => T[]; cost: (source: T, target: T) => number; }} graph
 * @param {T} start
 * @param {T} target
 * @returns {T[]} - the least-cost route, or undefined if there isn't any
 * @template T
 */
function dijkstra(graph, start, target) {
	/** @type {Map<T, number>} */
	const costs = new Map();
	/** @type {Set<T>} */
	const visited = new Set();
	/** @type {Map<T, T>} */
	const previous = new Map();
	costs.set(start, 0);
	var foundRoute = (start === target);
	var current = start;
	while(!foundRoute && current !== undefined) {
		/* recalculate costs to neighbors, and make sure to record the links in previous */
		for(const neighbor of graph.neighbors(current)) {
			if(!visited.has(neighbor)) {
				var tentativeDistance = costs.get(current) + graph.cost(current, neighbor);
				if(!costs.has(neighbor) || costs.get(neighbor) > tentativeDistance) {
					costs.set(neighbor, tentativeDistance);
					previous.set(neighbor, current);
				}
			}
		}
		visited.add(current);
		/* search for an unvisited node with the smallest cost */
		current = undefined;
		var smallestCost = Infinity;
		for(const [node, cost] of costs) {
			if(!visited.has(node)) {
				if(current === undefined || cost < smallestCost) {
					current = node;
					smallestCost = cost;
				}
			}
		}
		foundRoute = (current === target);
	}
	/* If we found something, reconstruct the route from the "previous" map */
	if(foundRoute) {
		var route = [target];
		while(previous.has(route[0])) {
			route.unshift(previous.get(route[0]));
		}
		return route;
	} else {
		return undefined;
	}
}

/**
 * @param {{ neighbors: (note: T) => T[]; cost: (source: T, target: T) => number; }} graph
 * @param {T} start
 * @param {T} target
 * @returns {T[]} - the least-cost route, or undefined if there isn't any
 * @template T
 */
function biheapDijkstra(graph, start, target) {
	/** @type {Map<T, number>} */
	const costs = new Map();
	/** @type {Set<T>} */
	const visited = new Set();
	/** @type {Map<T, T>} */
	const previous = new Map();
	const open = new setup.BinaryHeap((node) => costs.get(node));
	costs.set(start, 0);
	var foundRoute = (start === target);
	var current = start;
	while(!foundRoute && current !== undefined) {
		/* recalculate costs to neighbors, and make sure to record the links in previous */
		for(const neighbor of graph.neighbors(current)) {
			if(!visited.has(neighbor)) {
				var tentativeDistance = costs.get(current) + graph.cost(current, neighbor);
				if(!costs.has(neighbor)) {
					costs.set(neighbor, tentativeDistance);
					open.push(neighbor);
					previous.set(neighbor, current);
				} else if(costs.get(neighbor) > tentativeDistance) {
					costs.set(neighbor, tentativeDistance);
					open.rescore(neighbor);
					previous.set(neighbor, current);
				}
			}
		}
		visited.add(current);
		/* search for an unvisited node with the smallest cost */
		current = open.pop();
		foundRoute = (current === target);
	}
	/* If we found something, reconstruct the route from the "previous" map */
	if(foundRoute) {
		var route = [target];
		while(previous.has(route[0])) {
			route.unshift(previous.get(route[0]));
		}
		return route;
	} else {
		return undefined;
	}
}

setup.dijkstra = dijkstra;
setup.biheapDijkstra = biheapDijkstra;
