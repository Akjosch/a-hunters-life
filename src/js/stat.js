// @ts-check
const Stat = (function() {
	const _base = Symbol("base value");
	const _current = Symbol("current value");
	const _mods = Symbol("modifiers");
	const _totalMods = Symbol("combined modifiers");
	
	/**
	 * @param {Stat} stat 
	 */
	function _clearCache(stat) {
		stat[_current] = undefined;
	}

	/**
	 * @param {Stat} stat 
	 */
	function _calcTotalMods(stat) {
		/* gather multipliers */
		var mult = Object.values(stat[_mods])
			.map(function (m) { return Number.isFinite(m.mult) ? m.mult : 0; })
			.reduce(function (sum, add) { return sum + add; }, 0);
		var add = Object.values(stat[_mods])
			.map(function (m) { return Number.isFinite(m.add) ? m.add : 0; })
			.reduce(function (sum, add) { return sum + add; }, 0);
		stat[_totalMods] = {add: add, mult: mult};
	}

	class Stat {
		/**
		 * @param {Stat|object=} other - The Stat value to clone/copy.
		 */
		constructor(other) {
			if(other instanceof Stat) {
				/** @type {number} */
				this[_base] = other[_base];
				/** @type {Object.<string, {add: number, mult: (number|undefined)}>} */
				this[_mods] = clone(other[_mods]);
				/** @type {number} */
				this[_current] = other[_current];
				/** @type {{add: number, mult: number}} */
				this[_totalMods] = clone(other[_totalMods]);
			} else if (typeof other === "object") {
				/** @type {number} */
				this[_base] = Number(other.base);
				if(!Number.isFinite(this[_base])) { this[_base] = 0; }
				/** @type {Object.<string, {add: number, mult: (number|undefined)}>} */
				this[_mods] = (typeof other.mods === "object" ? clone(other.mods) : {});
				/** @type {number} */
				this[_current] = undefined;
				/** @type {{add: number, mult: number}} */
				this[_totalMods] = undefined;
			} else {
				/** @type {number} */
				this[_base] = 0;
				/** @type {Object.<string, {add: number, mult: (number|undefined)}>} */
				this[_mods] = {};
				/** @type {number} */
				this[_current] = undefined;
				/** @type {{add: number, mult: number}} */
				this[_totalMods] = undefined;
			}
		}

		/**
		 * @param {string} id
		 * @param {number|{add: number, mult: (number|undefined)}} mod
		 */
		addMod(id, mod) {
			id = String(id);
			if(Number.isFinite(mod)) {
				// @ts-ignore
				mod = { add: mod };
			}
			if(typeof mod === "object") {
				this[_mods][id] = mod;
				_clearCache(this);
			}
		}

		/**
		 * @param {String} id
		 */
		removeMod(id) {
			delete this[_mods][String(id)];
			_clearCache(this);
		}

		clearMods() {
			this[_mods] = {};
			_clearCache(this);
		}

		/**
		 * @returns {number}
		 */
		get base() {
			return this[_base];
		}

		set base(newBase) {
			newBase = Number(newBase);
			if(!Number.isFinite(newBase)) {
				newBase = 0;
			}
			if(this[_base] !== newBase) {
				this[_base] = newBase;
				_clearCache(this);
			}
		}

		/**
		 * @returns {Object.<string, {add: number, mult: (number|undefined)}>} 
		 */
		get mods() {
			return this[_mods];
		}

		/**
		 * @returns {number}
		 */
		get current() {
			if(this[_current] === undefined) {
				_calcTotalMods(this);
				this[_current] = this.base * (this[_totalMods].mult + 1) + this[_totalMods].add;
			}
			return this[_current];
		}

		/**
		 * @returns {string}
		 */
		toString() {
			_calcTotalMods(this);
			return this.current.toFixed(2)
				+ " [" + this.base.toFixed(2)
				+ " x " + (this[_totalMods].mult + 1).toFixed(2)
				+ (this[_totalMods].add >= 0 ? " + " + this[_totalMods].add.toFixed(2) : " - " + (-this[_totalMods].add).toFixed(2))
				+ "]";
		}

		/**
		 * @returns {Stat}
		 */
		clone() {
			return new Stat(this);
		}

		/**
		 * @returns {string|[string, any]}
		 */
		toJSON() {
			return JSON.reviveWrapper('new setup.Stat($ReviveData$)', {base: this[_base], mods: clone(this[_mods])});
		}
	}

	return Stat;
})();

setup.Stat = Stat;