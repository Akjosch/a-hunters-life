/* eslint-disable no-unused-vars */
// @ts-check
class Sprite {
	/**
	 * @param {string} tex
	 * @param {number[]} rect
	 */
	constructor(tex, rect) {
		this.tex = String(tex);
		if(Array.isArray(rect)) {
			if(rect.length != 4) {
				throw new Error("new Sprite(): rect parameter needs to be exactly four numbers long - [x, y, width, height]");
			}
			this.x = Number(rect[0]);
			this.y = Number(rect[1]);
			this.w = Number(rect[2]);
			this.h = Number(rect[3]);
			this.cropped = true;
		}
		else {
			this.cropped = false;
		}
		this.loaded = false;
		/** @type {HTMLImageElement} */
		this.img = undefined;
		var sp = this;
		/** @type {Promise<Sprite>} */
		this.read = new Promise(function (resolve, reject) {
			sp.img = new Image();
			sp.img.setAttribute('crossOrigin', 'anonymous');
			sp.img.onload = function () {
				if(!sp.cropped) {
					sp.x = 0;
					sp.y = 0;
					sp.w = sp.img.width;
					sp.h = sp.img.height;
				}
				sp.loaded = true;
				resolve(sp);
			};
			sp.img.src = sp.tex;
		});
	}
    /**
     * @this {Sprite}
     * @param {CanvasRenderingContext2D | HTMLCanvasElement | string} ctx
     * @param {number} [x=0]
     * @param {number} [y=0]
     * @returns {Promise<CanvasRenderingContext2D>} a Promise that's resolved when the drawing finishes
     */
	draw(ctx, x, y) {
		var context = getContext(ctx);
		x = Number(x) || 0;
		y = Number(y) || 0;
		var sprite = this;
		return this.read.then(function () {
			context.drawImage(sprite.img, sprite.x, sprite.y, sprite.w, sprite.h, x, y, sprite.w, sprite.h);
			return context;
		});
	}
    /**
     * @param {CanvasRenderingContext2D | HTMLCanvasElement | string} ctx
     * @param {number} [x=0]
     * @param {number} [y=0]
     * @param {number} [scale=1.0]
     * @returns {Promise<CanvasRenderingContext2D>} a Promise that's resolved when the drawing finishes
     */
	drawScaled(ctx, x, y, scale) {
		var context = getContext(ctx);
		x = Number(x) || 0;
		y = Number(y) || 0;
		scale = Number(scale) || 1.0;
		var sprite = this;
		return this.read.then(function () {
			context.drawImage(sprite.img, sprite.x, sprite.y, sprite.w, sprite.h, x, y, sprite.w * scale, sprite.h * scale);
			return context;
		});
	}
    /**
     * Cropped Sprites only work after being loaded.
     * @returns {string} a HTML image element ready to be inserted into a page
     */
	image() {
		if(!this.cropped) {
			return "<img src=" + JSON.stringify(this.tex) + " />";
		}
		else {
			return "<img src=" + JSON.stringify(this.canvas().toDataURL()) + " />";
		}
	}
    /**
     * Only works after the Sprite is loaded.
     * @returns {HTMLCanvasElement} A canvas element filled with the image
     */
	canvas() {
		const canvas = document.createElement("canvas");
		canvas.width = this.w;
		canvas.height = this.h;
		this.draw(canvas.getContext("2d"), 0, 0);
		return canvas;
	}
    /**
     * @returns {*} SugarCube JSON representation for serialisation
     */
	toJSON() {
		// @ts-ignore
		return JSON.reviveWrapper("setup.Sprite.create("
			+ JSON.stringify(this.tex)
			+ (this.cropped
				? ", [" + this.x + "," + this.y + "," + this.w + "," + this.h + "])"
				: ")"));
	}
    /**
     * @returns {Sprite} a copy of the Sprite instance
     */
	clone() {
		return new Sprite(this.tex, this.cropped ? [this.x, this.y, this.w, this.h] : undefined);
	}
    /**
     * @param {string} tex
     * @param {number[]} [rect]
     */
	static create(tex, rect) {
		return new Sprite(tex, rect);
	}
}

/**
 * Helper method to get a canvas object out of a string ID or HTMLCanvasElement element
 * @param {CanvasRenderingContext2D | HTMLCanvasElement | string} ctx
 * @returns {CanvasRenderingContext2D} the corresponding canvas object, or undefined
 */
function getContext(ctx) {
	/** @type {CanvasRenderingContext2D} */
	var result = undefined;
	if(typeof ctx === "string") {
		ctx = /** @type {HTMLCanvasElement} */ (document.getElementById(ctx));
	}
	if(ctx instanceof HTMLCanvasElement) {
		result = ctx.getContext("2d");
	} else if(ctx instanceof CanvasRenderingContext2D) {
		result = ctx;
	}
	return result;
}

setup.Sprite = Sprite;
