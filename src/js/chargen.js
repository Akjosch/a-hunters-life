var Sex = Object.freeze({UNKNOWN: 0, MALE: 1, FEMALE: 2});
setup.Sex = Sex;

var Race = Object.freeze({UNKNOWN: "", HESCIMAN: "hesciman", ELADRIM: "eladrim"});
setup.Race = Race;

var raceData = Object.freeze({
	hesciman: {
		heightAvg: [1.690, 1.753, 1.626],
		heightSigma: [0.083, 0.083, 0.082],
		weightMult: [18.077, 18.240, 17.913],
		complexion: {light: 1, medium: 4, dark: 1},
		skin: {
			light: {light: 6, fair: 2, pale: 1},
			medium: {light: 1, medium: 6, tanned: 2},
			dark: {medium: 1, tanned: 2, dark: 6, olive: 1}
		},
		hair: {
			light: {white: 1, blonde: 2*3+3, red: 1*3+3, auburn: 3*2+2, "chestnut brown": 9, brown: 11/3},
			medium: {blonde: 2, red: 1, auburn: 3, "chestnut brown": 9, brown: 11, black: 10},
			dark: {red: 1/3, auburn: 3/2, "chestnut brown": 9/2, brown: 11, black: 10}
		},
		eyes: {
			light: {steel: 1*3+3, green: 2*3+3, amber: 1*2+2, hazel: 2*2+2, blue: 3*3+3, "blue-green": 1*3+3, brown: 17/3, pale: 1, "ice blue": 1, grey: 1, "grey-green": 1},
			medium: {steel: 1/2, green: 2/2, amber: 1, hazel: 2, blue: 3/2, "blue-green": 1/2, black: 9/2, brown: 17/2},
			dark: {green: 2/3, amber: 1/2, hazel: 2/2, blue: 3/3, "blue-green": 1/3, black: 9, brown: 17}
		}
	},
	eladrim: {
		heightAvg: [1.828, 1.854, 1.803],
		heightSigma: [0.075, 0.083, 0.067],
		weightMult: [11.519, 11.570, 11.468],
		complexion: {light: 8, medium: 3, dark: 1},
		skin: {
			light: {light: 6, fair: 7, pale: 3},
			medium: {light: 3, medium: 13, tanned: 2},
			dark: {medium: 2, tanned: 3, dark: 1, olive: 2}
		},
		hair: {
			light: {white: 5, blonde: 20, red: 9, auburn: 4, "chestnut brown": 3, brown: 2},
			medium: {blonde: 9, red: 3, auburn: 4, "chestnut brown": 9, brown: 5, black: 1},
			dark: {red: 1, auburn: 4, "chestnut brown": 11, brown: 7, black: 2}
		},
		eyes: {
			light: {steel: 1*3+3, green: 2*3+3, amber: 1*2+2, hazel: 2*2+2, blue: 3*3+3, "blue-green": 1*3+3, brown: 17/3, pale: 1, "ice blue": 1, grey: 1, "grey-green": 1},
			medium: {steel: 1/2, green: 2/2, amber: 1, hazel: 2, blue: 3/2, "blue-green": 1/2, black: 9/2, brown: 17/2},
			dark: {green: 2/3, amber: 1/2, hazel: 2/2, blue: 3/3, "blue-green": 1/3, black: 9, brown: 17}
		}
	}

});

/**
 * @param {number} u
 * @param {number} v
 */
function normalDist1(u, v) {
	return Math.sqrt(-2.0 * Math.log(1 - u)) * Math.cos(2.0 * Math.PI * (1 - v));
}

/**
 * @param {number} u
 * @param {number} v
 */
function normalDist2(u, v) {
	return Math.sqrt(-2.0 * Math.log(1 - u)) * Math.sin(2.0 * Math.PI * (1 - v));
}

/* function skews [-3, 3] a bit so that skewVal(0, zero) = zero, skewVal(3, x) = 3 and skewVal(-3, x) = -3*/
/* uses a piecewise cubic function */
function skewVal(val, zero) {
	if(val) {
		return (val > 0 ? 1 : -1) * zero * val * val * val / 27 - zero * val * val * 2 / 9 + val + zero;
	}
	return zero;
}

/* makes a function more center-heavy */
function centerVal(val, amount) {
	return val * (amount * val * val - amount + 1);
}

function weightedVal(val, weights) {
	var pickVal = Object.values(weights).reduce((a, b) => a + b, 0) * val;
	var pick = Object.keys(weights).find((v) => (pickVal -= weights[v]) <= 0);
	return pick || val.random();
}

class Character {
	constructor(id) {
		if(!id || typeof id !== "string") {
			id = Math.round(Date.now() * (1 + Math.random())).toString(36).padStart(9, "0").substring(1);
		}
		this.id = id;
		/** @type {() => number} */
		this._rnd =  new Math.seedrandom(id);
		/** @type {number[]} */
		this._rndPool = [];
	}

	/**
	 * Used indices:
	 * 0 = sex, 1 = race, 2 = body, 3 = mind, 4+5 = height, 6 = weightMult,
	 * 7 = skin, 8 = hair, 9 = eyes
	 * 10+11 = agility, 12+13 = dexterity, 14+15 = recuperation, 16+17 = speed, 18+19 = strength,
	 * 20+21 = toughness, 22+23 = willpower, 24+25 = magic,
	 * 26+27 = hearing, 28+29 = vision
	 * 30 = complexion
	 * 31+32 = smelling, 33+34 = charisma, 35+36 = intelligence, 37+38 = intuition, 39+40 = luck
	 * 41+42 = appearance
	 * 
	 * @param {number} idx
	 * @returns {number}
	 */
	_getRandom(idx) {
		while(this._rndPool.length <= idx) {
			this._rndPool.push(this._rnd());
		}
		return this._rndPool[idx];
	}

	/**
	 * Warning: uses [idx] and [idx+1] indexes.
	 * @param {number} idx
	 * @param {boolean} [second] - use the second number
	 * @returns {number}
	 */
	_getNormalRandom(idx, second) {
		return (second ? normalDist2 : normalDist1)(this._getRandom(idx), this._getRandom(idx + 1));
	}

	get sex() {
		return (this._getRandom(0) < 0.5 ? Sex.MALE : Sex.FEMALE);
	}

	/* TODO: Use weighted random */
	get race() {
		var rid = this._getRandom(1);
		if(rid < 0.5) {
			return Race.HESCIMAN;
		} else if(rid < 0.51) {
			return Race.ELADRIM;
		}
		return Race.UNKNOWN;
	}

	get body() {
		return this._getRandom(2) * 200 - 100;
	}

	get mind() {
		return this._getRandom(3) * 200 - 100;
	}

	get spirit() {
		return (1 - this._getRandom(2) - this._getRandom(3)) * 100;
	}

	get height() {
		var data = raceData[this.race];
		if(data) {
			return skewVal(this._getNormalRandom(4), 2 * this._getRandom(2) - 1) * data.heightSigma[this.sex] + data.heightAvg[this.sex];
		}
		return NaN;
	}

	get weightMult() {
		return Math.pow(2, Math.pow(this._getRandom(2) + this._getRandom(6) - 1, 5));
	}

	get weight() {
		var data = raceData[this.race];
		if(data) {
			return Math.pow(this.height, 2.6) * data.weightMult[this.sex] * this.weightMult;
		}
		return NaN;
	}

	get bmi() {
		return this.weight / this.height / this.height;
	}

	get complexion() {
		var data = raceData[this.race];
		if(data) {
			return weightedVal(this._getRandom(30), data.complexion);
		}
		return "";
	}

	get skin() {
		var data = raceData[this.race];
		if(data) {
			return weightedVal(this._getRandom(7), data.skin[this.complexion]);
		}
		return "";
	}

	get hair() {
		var data = raceData[this.race];
		if(data) {
			return weightedVal(this._getRandom(8), data.hair[this.complexion]);
		}
		return "";
	}

	get eyes() {
		var data = raceData[this.race];
		if(data) {
			return weightedVal(this._getRandom(9), data.eyes[this.complexion]);
		}
		return "";
	}

	get health() {
		return this.body / 4 + 75;
	}

	get stamina() {
		return Math.sqrt(Math.max(25, this.stats.toughness + this.body + 200)) * 10;
	}

	/**
	 * @param {number} idx
	 * @param {number} skew
	 */
	_getStat(idx, skew) {
		return (skewVal(this._getNormalRandom(idx, false), skew) * 30).clamp(-100, 100);
	}

	get stats() {
		if(this._stats) {
			return this._stats;
		} else {
			var _body = 2 * this._getRandom(2) - 1;
			var _mind = 2 * this._getRandom(3) - 1;
			var _spirit = 1 - this._getRandom(2) - this._getRandom(3);
			this._stats = {
				agility: this._getStat(10, _body),
				dexterity: this._getStat(12, _body),
				recuperation: this._getStat(14, ( _body + _mind) / 2),
				speed: this._getStat(16, (_body + _mind) / 2),
				strength: this._getStat(18, _body),
				toughness: this._getStat(20,  centerVal(_body, 0.8)),
				willpower: this._getStat(22, centerVal(_mind, 0.4)),
				magic: this._getStat(24, (_mind + _spirit) / 2),
				hearing: this._getStat(26, _mind),
				vision: this._getStat(28, _mind),
				smelling: this._getStat(31, _mind),
				charisma: this._getStat(33, _mind),
				intelligence: this._getStat(35, _mind),
				intuition: this._getStat(37, (_mind + _spirit) / 2),
				luck: this._getStat(39, 0),
				appearance: this._getStat(41, _body / 2)
			}
			this._stats._average = Object.values(this._stats).reduce(function(a, b) { return a + b; }) / Object.keys(this._stats).length;
			this._stats._min = Math.min.apply(null, Object.values(this._stats));
			this._stats._max = Math.max.apply(null, Object.values(this._stats));
			return this._stats;
		}
	}

	_getStatMax(idx) {
		return Math.abs((this._getNormalRandom(idx, true) * 30).clamp(-100, 100));
	}

	get statsMax() {
		if(this._statsMax) {
			return this._statsMax;
		} else {
			this._statsMax = clone(this.stats);
			delete this._statsMax._average;
			delete this._statsMax._min;
			delete this._statsMax._max;
			this._statsMax.agility += this._getStatMax(10);
			this._statsMax.dexterity += this._getStatMax(12);
			this._statsMax.recuperation += this._getStatMax(14);
			this._statsMax.speed += this._getStatMax(16);
			this._statsMax.strength += this._getStatMax(18);
			this._statsMax.toughness += this._getStatMax(20);
			this._statsMax.willpower += this._getStatMax(22);
			this._statsMax.magic += this._getStatMax(24);
			this._statsMax.hearing += this._getStatMax(26);
			this._statsMax.vision += this._getStatMax(28);
			this._statsMax.smelling += this._getStatMax(31);
			this._statsMax.charisma += this._getStatMax(33);
			this._statsMax.intelligence += this._getStatMax(35);
			this._statsMax.intuition += this._getStatMax(37);
			this._statsMax.luck += this._getStatMax(39) * 0.3;
			this._statsMax.appearance += this._getStatMax(41) * 0.5;
			return this._statsMax;
		}
	}

	toJSON() {
		return JSON.reviveWrapper("new setup.Character($ReviveData$)", this.id);
	}

	clone() {
		return new Character(this.id);
	}

	static newSequentialGen() {
		var currentSeed = NaN;
		return function() {
			if(Number.isNaN(currentSeed)) {
				currentSeed = Math.round(Date.now() * (1 + Math.random()));
			} else {
				++ currentSeed;
			}
			return new Character(currentSeed.toString(36).padStart(9, "0").substring(1));
		};
	}

	/**
	 * Cummulative functions:
	 *
 	 * f_0(x) = x + 1 (for x below inherent)
 	 * f_1(x) = 1/2 x^2 + x + 1 (for x between inherent and max)
 	 * f_2(x) = 1/4 ((a + 1) / a^3 * x^4 + a (a + 3)) + 1 (for x above max, with a = max - inherent)
	 *
 	 * @param {number} x
 	 * @param {number} a
 	 */
	static _costPoint(x, a) {
		if(x <= 0) {
			return (x + 1);
		} else if(x <= a) {
			return (x * x / 2 + x + 1);
		} else {
			return (((a + 1) / a / a / a * x * x * x * x + a * (a + 3)) / 4 + 1);
		}
	}

	/**
	 * Reverse cost functions:
	 * f^-1_0(x) = x - 1
	 * f^-1_1(x) = (2x - 1)^0.5 - 1
	 * f^-1_2(x) = (a^3 ( 4x - a^2 - 3a - 4) / (a + 1) )^0.25
	 * 
	 * @param {number} c 
	 * @param {number} a 
	 */
	static _reverseCost(c, a) {
		if(c <= 1) {
			return (c - 1);
		} else if(c <= Math.sqrt(2 * a - 1) - 1) {
			return (Math.sqrt(2 * c - 1) - 1);
		} else {
			return Math.pow(a * a * a * (4 * c - a * a - 3 * a - 4) / (a + 1), 0.25);
		}
	}

	static expCost(from, to, inherent = 0, max = 50) {
		if(!Number.isFinite(from) || !Number.isFinite(to) || !Number.isFinite(inherent) || !Number.isFinite(max)) {
			throw new TypeError("All arguments have to be finite numbers");
		}
		if(to < from) {
			throw new TypeError("Target value (" + to + ") has to be larger than or equal to start value (" + from + ")");
		}
		if(max <= inherent) {
			throw new TypeError("Inherent value (" + inherent + ") has to be larger than the max value (" + max + ").");
		}
		return Character._costPoint(to - inherent, max - inherent) - Character._costPoint(from - inherent, max - inherent);
	}
}
setup.Character = Character;
