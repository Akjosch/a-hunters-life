var asserts = (function() {
	/**
	 * @param {any} s
	 * @param {number} n
	 */
	function truncate(s, n) {
		if (typeof s === 'string') {
			return s.length < n ? s : s.slice(0, n);
		} else {
			return s;
		}
	}

	/**
	 * @param {{ message: string; actual: any; expected: any; operator: string; }} options
	 */
	var AssertionError = function AssertionError(options) {
		this.name = "AssertionError";
		this.actual = options.actual;
		this.expected = options.expected;
		this.operator = options.operator;
		if(options.message) {
			this.message = options.message;
			this.generatedMessage = false;
		} else {
			this.message =  truncate("" + this.actual, 128) + ' ' + this.operator + ' ' + truncate("" + this.expected, 128);
			this.generatedMessage = true;
		}
	};
	AssertionError.prototype = Object.create(Error.prototype, {
		constructor: {
			value: AssertionError,
			enumerable: false,
			writable: true,
			configurable: true
		}
	});
	/**
	 * @param {any} value
	 * @param {any} message
	 */
	function assert(value, message) {
		if(!value) {
			throw new AssertionError({
				message: message,
				actual: value,
				expected: true,
				operator: "=="			
			});
		}
	}
	/**
	 * @param {any} value
	 * @param {any} message
	 */
	function assertString(value, message) {
		return assert(typeof value === "string", message);
	}
	/**
	 * @param {any} value
	 * @param {any} message
	 */
	function assertNumber(value, message) {
		return assert(typeof value === "number", message);
	}
	/**
	 * @param {any} value
	 * @param {any} message
	 */
	function assertFinite(value, message) {
		return assert(Number.isFinite(value), message);
	}
	/**
	 * @param {any} value
	 * @param {any} message
	 */
	function assertArray(value, message) {
		return assert(value instanceof Array, message);
	}
	return {
		assert: assert,
		assertString: assertString,
		assertNumber: assertNumber,
		assertFinite: assertFinite,
		assertArray: assertArray
	};
})();

window.assert = asserts.assert;
window.assertString = asserts.assertString;
window.assertNumber = asserts.assertNumber;
window.assertFinite = asserts.assertFinite;
window.assertArray = asserts.assertArray;