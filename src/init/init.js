/* eslint-disable no-unused-vars */
Config.passages.nobr = true;
Config.macros.ifAssignmentError = false;

/* setting up requireJS config */
require.config({baseUrl: "ext/lib"});
/* Define builtins */
define("jquery", [], function() { return window.jQuery; });
define("imagesloaded", [], function() { return window.imagesLoaded; });
define("lz-string", [], function() { return window.LZString; });
define("file-saver", [], function() { return { saveAs: window.saveAs }; });
define("seedrandom", [], function() { return Math.seedrandom; });
define("sugarcube", [], function() { return window.SugarCube; });
define("sugarcube/dialog", [], function() { return Dialog; });
define("sugarcube/engine", [], function() { return Engine; });
define("sugarcube/fullscreen", [], function() { return Fullscreen; });
define("sugarcube/macro", [], function() { return Macro; });
define("sugarcube/passage", [], function() { return Passage; });
define("sugarcube/save", [], function() { return Save; });
define("sugarcube/scripting", [], function() { return Scripting; });
define("sugarcube/setting", [], function() { return Setting; });
define("sugarcube/simple-audio", [], function() { return SimpleAudio; });
define("sugarcube/state", [], function() { return State; });
define("sugarcube/story", [], function() { return Story; });
define("sugarcube/ui", [], function() { return UI; });
define("sugarcube/ui-bar", [], function() { return UIBar; });
define("sugarcube/util", [], function() { return Util; });
define("sugarcube/wikifier", [], function() { return Wikifier; });

window.loadModule = function(mod, namespace, name) {
	name = name || mod.toUpperFirst();
	namespace = namespace || setup;
	return new Promise(function(resolve, reject) {
		require([mod], function(result) {
			namespace[name] = result;
			resolve(result);
		});
	});
};

/* Bunch of ease-of-use defines */
Object.defineProperty(window, "v", {
	get: function() {
		return State.variables;
	}
});
Object.defineProperty(window, "t", {
	get: function() {
		return State.temporary;
	}
});
Object.defineProperty(window, "setup", {
	get: function() {
		return setup;
	}
});

/* menu manupilation */

jQuery(document).one(":storyready", function() {
	jQuery("#ui-bar-body").append("<div id='story-info'></div>");
	const $infoArea = jQuery("#story-info");
	if(Story.has("StoryInfo")) {
		const infoCode = Story.get("StoryInfo").processText();
		$infoArea.empty().wiki(infoCode);
		jQuery(document).on(":passageend", function() {
			$infoArea.empty().wiki(infoCode);
		});
	}
});