module.exports = function (api) {
	api.cache(true);

	const presets = [
		[
			"@babel/preset-env",
			{
				targets: {
					browsers: ["last 2 version"],
					ie: "11"
				},
				spec: true,
				useBuiltIns: false,
				corejs: 3,
				debug: true
			},
		]/*,
		[
			"babel-preset-minify",
			{
			"evaluate": false,
			"mangle": false
			},
		]*/
	];
	const plugins = [
	];

	console.log("LOADED");

	return {
		presets,
		plugins
	};
};
