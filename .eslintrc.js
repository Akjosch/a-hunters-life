module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
				"SharedArrayBuffer": "readonly",
				"setup": "readonly",
				"Config": "readonly",
				"State": "readonly",
				"Story": "readonly",
				"Macro": "readonly",
				"Scripting": "readonly",
				"jQuery": "readonly",
				"clone": "readonly",
				"importScripts": "readonly",
				"define": "readonly"
    },
    "parserOptions": {
			"sourceType": "module",
      "ecmaVersion": 2018
    },
    "rules": {
    }
};