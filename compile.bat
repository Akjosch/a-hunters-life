:: Basic Compiler - Windows

:: See if we can find a git installation
setlocal enabledelayedexpansion

set GITFOUND=no
for %%k in (HKCU HKLM) do (
    for %%w in (\ \Wow6432Node\) do (
        for /f "skip=2 delims=: tokens=1*" %%a in ('reg query "%%k\SOFTWARE%%wMicrosoft\Windows\CurrentVersion\Uninstall\Git_is1" /v InstallLocation 2^> nul') do (
            for /f "tokens=3" %%z in ("%%a") do (
                set GIT=%%z:%%b
				set GITFOUND=yes
                goto FOUND
            )
        )
    )
)
:FOUND
if %GITFOUND% == yes (
	set "PATH=%GIT%bin;%PATH%"
)

:: Run the appropriate compiler for the user's CPU architecture.
if %PROCESSOR_ARCHITECTURE% == AMD64 (
    CALL "%~dp0tools\tweego_win64.exe" --head=header.txt -o "%~dp0bin/ahl.html" "%~dp0src"
) else (
    CALL "%~dp0tools\tweego_win86.exe" --head=header.txt -o "%~dp0bin/ahl.html" "%~dp0src"
)
rmdir /S /Q "%~dp0bin/ext"
xcopy "%~dp0ext" "%~dp0bin/ext" /S /E /I

if %GITFOUND% == yes (
	:: Make the output prettier, replacing \t with a tab and \n with a newline
    bash -c "sed -i -e 's/^\s*//' bin/ahl.html"
)

echo Done
