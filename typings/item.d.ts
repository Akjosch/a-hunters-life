interface Item {
	long:string;
	short:string;
	icon:string;
	invisible:boolean;
	value:number;
	tags:string[];
	hasTag(tag:string):boolean;
	hasAnyTag(...tags:string):boolean;
	hasAct(act:string):boolean;
	validFor(ch:any):boolean;
	acts:{[x:string]:any};
	describe(ch:any, attr:string):string;
	act(ch:any, act:string):string;
	longDesc(ch:any):string;
	shortDesc(ch:any):string;
}