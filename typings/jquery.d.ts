interface JQuery {
	bindFirst(name:any, callback:any):void;
	ariaClick(options?:object, handler:any): JQuery;
	wiki(text:string): JQuery;
	wikiWithOptions(options:object, text:string): JQuery;
}

interface JQueryStatic {
	_data: any;
}

interface Window {
	jQuery:JQuery;
}