declare interface SugarCube2 {
}

declare interface StoryConstructor {
	lookup(propertyName:string, searchValue:(string|number), sortProperty?:string): Passage[];
	has(passageName:string): boolean;
}

declare var Story: StoryConstructor;

declare interface Passage {
	domId:string;
	tags:string[];
	text:string;
	title:string;
	description():string;
	processText():string;
}

declare interface StateConstructor {
	variables:any;
	temporary:any;
	hasPlayed(passage:string): boolean;
}

declare var State: StateConstructor;

declare interface MacroConstructor {
	add(name:(string|string[]), definition:(object|string), deep?:boolean): void;
	delete(): void;
	get(name:string): object;
	has(name:string): boolean;
}

declare var Macro: MacroConstructor;

interface Array {
	random(): T;
	last(): T;
	includesAny(val:any|array): boolean;
}

interface String {
	startsWith(value:string): boolean;
	endsWith(value:string): boolean;
	toUpperFirst(): string;
}

interface Math {
	seedrandom: any;
}

interface JSON {
	reviveWrapper(codeString: string, reviveData: any):[string, any];
}

declare function importScripts(urls:string | string[]): Promise;
declare function clone(original:any):any;

interface NumberConstructor {
	isFinite(value:any):boolean;
}

interface Number {
	clamp(min:number, max:number):number;
}

interface Window {
	SugarCube: SugarCube2;
}
